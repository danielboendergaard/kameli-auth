<?php namespace Kameli\Auth\Facade;

use Illuminate\Support\Facades\Facade;

class AuthManager extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'Kameli\Auth\AuthManager'; }
} 