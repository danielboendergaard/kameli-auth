<?php namespace Kameli\Auth;

class AuthManager {

    /**
     * @var array
     */
    protected $instances;

    /**
     * @param $name
     * @return \Kameli\Auth\Adapter\AdapterInterface
     */
    public function type($name)
    {
        if ( ! isset($this->instances[$name]))
        {
            throw new \InvalidArgumentException('Instance '.$name.' has not been registered');
        }

        return $this->instances[$name];
    }

    /**
     * @param \Kameli\Auth\Adapter\AdapterInterface $instances
     */
    public function register($instances)
    {
        if ( ! is_array($instances)) $instances = func_get_args();

        foreach ($instances as $instance)
        {
            /** @var \Kameli\Auth\Adapter\AdapterInterface $instance */
            $this->instances[$instance->getName()] = $instance;
        }
    }
}