<?php namespace Kameli\Auth\Adapter;

use Cartalyst\Sentry\Sentry;
use Illuminate\Events\Dispatcher;
use Kameli\Auth\Exception\PasswordInvalidException;
use Kameli\Auth\Exception\UserNotActivatedException;
use Kameli\Auth\Exception\UserNotFoundException;

class SentryTwoAdapter extends AbstractAdapter {

    /**
     * @var \Cartalyst\Sentry\Sentry
     */
    protected $sentry;

    /**
     * @param string $name
     * @param \Illuminate\Events\Dispatcher $events
     * @param \Cartalyst\Sentry\Sentry $sentry
     */
    public function __construct($name, Dispatcher $events, Sentry $sentry)
    {
        $this->name = $name;
        $this->events = $events;
        $this->sentry = $sentry;
    }

    /**
     * Register a new user
     * @param array $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function register(array $attributes, $activate = false)
    {
        $user = $this->sentry->register($attributes, $activate);

        $this->fireRegistrationEvent($user);

        return $user;
    }

    /**
     * Activate a user
     * @param string $activationCode
     * @param array $attributes
     * @throws \Kameli\Auth\Exception\PasswordInvalidException
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @return mixed
     */
    public function activate($activationCode, $attributes = null)
    {
        try {
            $user = $this->sentry->findUserByActivationCode($activationCode);
        } catch (UserNotFoundException $e) {
            throw new \Kameli\Auth\Exception\UserNotFoundException;
        }

        // If a password is provided, validate it
        if (isset($attributes['password']))
        {
            $passwordConfirmation = isset($attributes['password_confirmation']) ? $attributes['password_confirmation'] : null;

            if ( ! $this->validatePassword($attributes['password'], $passwordConfirmation))
            {
                throw new PasswordInvalidException;
            }
        }

        // Activate the user
        $user->attemptActivation($user->getActivationCode());

        // Update the user with the provided attributes
        if ($attributes) {
            $user->fill($attributes);
            $user->password = $attributes['password'];
            $user->save();
        }

        return $user;
    }

    /**
     * Authenticate a user
     * @param array $credentials
     * @param bool $remember
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     */
    public function authenticate(array $credentials, $remember = false)
    {
        try {
            return $this->sentry->authenticate($credentials, $remember);
        } catch (\Exception $e) {
            throw new UserNotFoundException;
        }
    }

    /**
     * Log a user in
     * @param \Illuminate\Database\Eloquent\Model $user
     * @param bool $remember
     * @throws \Kameli\Auth\Exception\UserNotActivatedException
     */
    public function login($user, $remember = false)
    {
        try {
            $this->sentry->login($user, $remember);
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            throw new UserNotActivatedException;
        }
    }

    /**
     * Log out the current user
     */
    public function logout()
    {
        $this->sentry->logout();
    }

    /**
     * Check if a user is logged in
     * @return boolean
     */
    public function check()
    {
        return $this->sentry->check();
    }

    /**
     * Get the current user
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function user()
    {
        return $this->sentry->getUser();
    }

    /**
     * @param string $credentials
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @throws \Kameli\Auth\Exception\UserNotActivatedException
     */
    public function remind($credentials)
    {
        try {
            $user = $this->sentry->findUserByCredentials($credentials);
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            throw new UserNotFoundException;
        }

        if ( ! $user->isActivated()) throw new UserNotActivatedException;

        $this->fireReminderEvent($user);
    }

    /**
     * Check that the password reset code is valid
     * @param string $code
     * @return \Cartalyst\Sentry\Users\UserInterface
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     */
    public function findUserByResetPasswordCode($code)
    {
        try {
            return $this->sentry->findUserByResetPasswordCode($code);
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            throw new UserNotFoundException;
        }
    }

    /**
     * Attempt a password reset
     * @param string $code
     * @param string $newPassword
     * @param string $confirmation
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @throws \Kameli\Auth\Exception\PasswordInvalidException
     */
    public function attemptPasswordReset($code, $newPassword, $confirmation = null)
    {
        $user = $this->findUserByResetPasswordCode($code);

        if ( ! $this->validatePassword($newPassword, $confirmation))
        {
            throw new PasswordInvalidException;
        }

        $user->attemptResetPassword($code, $newPassword);

        return $user;
    }

    /**
     * Find a user by its activation code
     * @param string $code
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     */
    public function findUserByActivationCode($code)
    {
        try {
            return $this->sentry->findUserByActivationCode($code);
        } catch (\Exception $e) {
            throw new \Kameli\Auth\Exception\UserNotFoundException;
        }
    }
}