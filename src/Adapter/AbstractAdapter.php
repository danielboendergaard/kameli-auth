<?php namespace Kameli\Auth\Adapter;

abstract class AbstractAdapter implements AdapterInterface {

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Illuminate\Events\Dispatcher
     */
    protected $events;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Fire the registration event
     * @param \Illuminate\Database\Eloquent\Model $user
     */
    protected function fireRegistrationEvent($user)
    {
        $this->events->fire("auth.registered:{$this->getName()}", $user);
    }

    /**
     * Register a registration event
     * @param $listener
     */
    public function registerRegistrationEvent($listener)
    {
        $this->events->listen("auth.registered:{$this->getName()}", $listener);
    }

    /**
     * Fire the reminder event
     * @param \Illuminate\Database\Eloquent\Model $user
     */
    protected function fireReminderEvent($user)
    {
        $this->events->fire("auth.remind:{$this->getName()}", $user);
    }

    /**
     * Register a reminder event
     * @param $listener
     */
    public function registerReminderEvent($listener)
    {
        $this->events->listen("auth.remind:{$this->getName()}", $listener);
    }

    /**
     * Validate a new password
     * @param string $newPassword
     * @param string $confirmation
     * @return bool
     */
    protected function validatePassword($newPassword, $confirmation = null)
    {
        if ( ! is_null($confirmation) && $newPassword !== $confirmation) return false;

        return strlen($newPassword) >= 6;
    }
}