<?php namespace Kameli\Auth\Adapter;

interface AdapterInterface {

    /**
     * @return string
     */
    public function getName();

    /**
     * Register a new user
     * @param array $attributes
     * @param bool $activate
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function register(array $attributes, $activate = false);

    /**
     * Activate a user
     * @param string $activationCode
     * @param array $attributes
     * @throws \Kameli\Auth\Exception\PasswordInvalidException
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @return mixed
     */
    public function activate($activationCode, $attributes = null);

    /**
     * Authenticate a user
     * @param array $credentials
     * @param bool $remember
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     */
    public function authenticate(array $credentials, $remember = false);

    /**
     * Log a user in
     * @param \Illuminate\Database\Eloquent\Model $user
     * @param bool $remember
     * @throws \Kameli\Auth\Exception\UserNotActivatedException
     */
    public function login($user, $remember = false);

    /**
     * Log out the current user
     */
    public function logout();

    /**
     * Check if a user is logged in
     * @return boolean
     */
    public function check();

    /**
     * Get the current user
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function user();

    /**
     * @param array $credentials
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @throws \Kameli\Auth\Exception\UserNotActivatedException
     */
    public function remind($credentials);

    /**
     * Check that the password reset code is valid
     * @param string $code
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findUserByResetPasswordCode($code);

    /**
     * Attempt a password reset
     * @param string $code
     * @param string $newPassword
     * @param string $confirmation
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     * @throws \Kameli\Auth\Exception\PasswordInvalidException
     */
    public function attemptPasswordReset($code, $newPassword, $confirmation = null);

    /**
     * Find a user by its activation code
     * @param string $code
     * @throws \Kameli\Auth\Exception\UserNotFoundException
     */
    public function findUserByActivationCode($code);
}